% Test file

c = addTwoNumbers_myName(1, 2)

% test if the addition function works as expected
assert(c == 3)

d = multiplyTwoNumbers_myName(3, 3)

%test if the multiplication function works as expected
assert(d == 9)

% Test for the homework
% test my sqrt function
assert(sqrt_Beatriz(25) == sqrt(25))
