function c = multiplyTwoNumbers_myName(a, b)
% multiplyTwoNumbers_myName(a, b) returns the product of a and b

c = a * b;
end
